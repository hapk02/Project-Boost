using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] float mainThrust = 1f;
    [SerializeField] float rotateThrust = 1f;
    [SerializeField] AudioClip mainEngine;
    [SerializeField] ParticleSystem mainBoosterParticles;
    [SerializeField] ParticleSystem leftBoosterParticles;
    [SerializeField] ParticleSystem rightBoosterParticles;

    Rigidbody rb;
    AudioSource audioSource;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        ProcessThrust();
        ProcessRotation();
    }

    void ProcessThrust(){
        if (Input.GetKey(KeyCode.Space))
        {
            //Thrust
            StartThrusting();
        }
        else
        {
            StopThrustSFX();
        }

    }

    void ProcessRotation(){
        
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            //Rotate left
            RotateLeft();

        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            //Rotate right
            RotateRight();
        }
        else
        {
            StopRotationSFX();
        }
    }

    private void StopThrustSFX()
    {
        mainBoosterParticles.Stop();
        audioSource.Stop();
    }

    private void StartThrusting()
    {
        Vector3 calculation = Vector3.up * mainThrust * Time.deltaTime;
        rb.AddRelativeForce(calculation);
        if (!mainBoosterParticles.isPlaying)
        {
            mainBoosterParticles.Play();
        }
        if (!audioSource.isPlaying)
        {
            audioSource.PlayOneShot(mainEngine);
        }
    }

    private void StopRotationSFX()
    {
        leftBoosterParticles.Stop();
        rightBoosterParticles.Stop();
    }

    private void RotateRight()
    {
        ApplyRotation(-rotateThrust);
        if (!leftBoosterParticles.isPlaying)
        {
            leftBoosterParticles.Play();
        }
    }

    private void RotateLeft()
    {
        ApplyRotation(rotateThrust);
        if (!rightBoosterParticles.isPlaying)
        {
            rightBoosterParticles.Play();
        }
    }

    void ApplyRotation(float rotationThisFrame){
        rb.freezeRotation = true; //freeze rotation to manually rotate
        transform.Rotate(Vector3.forward * rotationThisFrame * Time.deltaTime);
        rb.freezeRotation = false; //physx can take over
    }
}
